## Subir fastq al ENA

#### Galo, 19 Diciembre de 2019 (actualizado por Mariana 31/08/2023)

El proceso está basado en un script de Public Health England. El último commit
de GitHub fue en 2016, así que quizá en algún punto sea incompatible si actualizan
el ENA:

https://github.com/phe-bioinformatics/ena_submission

#### Preparación de los archivos
En primer lugar ponemos los fastq que queramos subir en una carpeta y los
renombramos de manera que queden:

* Sample.R1.fastq.gz
* Sample.R2.fastq.gz

Yo para esto uso el comando `mmv`

Ahora hay que generar un archivo que contenga la siguiente información:
`SAMPLE,TAXON_ID,SCIENTIFIC_NAME,DESCRIPTION,COLLECTION DATE,GEOGRAPHIC LOCATION (country and/or sea)`

Además de esa información mínima, se pueden añadir tantos otros campos como se
quiera. En el caso de "Date" es suficiente con poner el año, o 2008-01-23; 2008-01; 2008.

Para hacer la tabla en la carpeta donde estén ya los fastq renombrados correctamente:

```bash
echo "SAMPLE,TAXON_ID,SCIENTIFIC_NAME,DESCRIPTION,COLLECTION DATE,GEOGRAPHIC LOCATION (country and/or sea)" > Samples.info.csv
ls *R1.fastq.gz | cut -f1 -d'.' |
while read sample
  do
    echo "${sample},1773,Mycobacterium tuberculosis, dWGS of M. tuberculosis (culture-free sequencing from clinical samples), DATE, LOCATION"
  done >> Samples.info.csv
```
Sustituir DATE y LOCATION por los datos específicos
Por último falta un archivo con el título del proyecto y el abstract. Este archivo
, que llamaré `TitleAndAbstract.txt`, consta de *una sóla línea* separada por un tabulador:
`dWGS_of_MTB[TAB]Over the last years, tuberculosis has been a subject of...`

#### Subida al servidor "test" del ENA
Con todos los archivos listos, podemos ejecutar el script para subirlos. Este script
va a generar los archivos xml necesarios y va a subir las muestras al servidor de
*testeo* de ENA. De manera que tras ejecutarlo las secuencias todavía no estarán
disponibles. El proceso tarda porque tiene que subir los archivos, así que hay
que ejecutarlo con `screen` o `nohup`.

*NOTA*: por defecto sube las secuencias como Illumina HiSeq 2500, 100bp. Yo de
esto no me había dado cuenta. He incluido abajo los argumentos para cambiarlo
`--read_length 250` y `-m "Illumina MiSeq"` pero no los he usado nunca, así que
el script podría dar error ahí.

```bash
/usr/bin/python /data/Software/ena_submission/ena_submission.py \
  -d -i /ruta/a/la/carpeta/con/los/fastq/ \
  -r nombre_de_referencia \ # e.g dWGS_of_MTB
  -f /ruta/a/Samples.info.csv \ # el archivo que hemos generado antes
  -o /nombre/carpeta/output/ \ # aqui se guardan los xml que genera el script
  -a /ruta/a/TitleAndAbstract.txt \
  --read_length 250 \ # La longitud de lectura de los archivos
  -m "Illumina MiSeq" \ # Secuenciador
  -c IBV_CSIC \ # Nombre del centro
  -user Webin-49763 \ # El usuario de tuberculosisgenomicsunit (a nombre de Iñaki)
  -pass my_password # La contraseña
```

Tras ejecutarlo el script ha  generado la carpeta `output` con los archivos:
`experiment.xml`, `run.xml`, `sample.xml`, `study.xml`, `submission.xml` y, si
todo se ha subido sin problemas, `receipt.xml`.

Además, al terminar, el script te avisa de que todo se ha subido al servidor de
testeo, pero que para subir todo definitivamente al ENA, hay que ejecutar un comando
`curl` que aparece por pantalla. Antes de hacerlo, podemos comprobar en
`receipt.xml` que las secuencias no se van a subir como públicas `status=PRIVATE`.

******CURL NO FUNCIONA MÁS, USAR LA ACTUALIZACION DE ALVARO MENCIONADA ABAJO

Si hemos perdido el output y no tenemos el comando, podemos volver a sacarlo con:

```bash
/usr/bin/python /data/Software/ena_submission/ena_submission.py \
-curl -user Webin-49763 -pass my_password \
-o /nombre/carpeta/output/ # Donde estan los archivos xml
```


Una vez tengamos todo claro ejecutamos el comando que salió por pantalla.
Será algo como:

`curl -k -F "SUBMISSION=@submission.xml" -F "STUDY=@study.xml" -F "SAMPLE=@sample.xml" -F "EXPERIMENT=@experiment.xml" -F "RUN=@run.xml" "https://www.ebi.ac.uk/ena/submit/drop-box/submit/?auth=ENA%20"Webin-49763%20my_password`

Ahora ya estará todo subido y debería aparecer en nuestra cuenta del ENA como
datos privados con fecha de _release_ a un año vista.******

#### Álvaro, 21 Julio de 2022

Como Galo predijo, una parte del script ha dejado de funcionar tal y como está descrito arriba. Sin embargo aún se puede aprovechar parte. Lo que falla es el paso del `curl`.

Lo que hay que hacer, a fecha de hoy, es generar los XML y subir los FASTQ al ftp con el paso 'Subida al servidor "test" del ENA'. 

Luego, con los XML generados, en vez de hacer `curl` lo más facil es ir a la [web de submission de ENA](https://www.ebi.ac.uk/ena/submit/webin/login) y subir 'a mano' los XML. Para ello, hay que logearse con el usuario (Webin-XXX) que toque. Luego, en la sección "Studies (Projects)" pulsar el botón "Submit XMLs (Advanced)". Ahí subiremos los archivos `experiment.xml`, `run.xml`, `sample.xml`, `study.xml`, `submission.xml`. 

Luego le damos al botón 'Submit' y, si todo ha ido bien, ya tendremos nuestro projecto creado con los datos subidos. 
