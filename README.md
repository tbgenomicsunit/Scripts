**This is the README of the Scripts project from tbgenomicsunit**
<br />
<br />
* Master branch should only contain updated, tested, debuged, functional scripts
* This readme must contain the name of each script, its purpose and how to use it
<br />
<br />
**USER COMMENTS**
<br />
<br />
GALO: All my custom scripts are named as "Script-xxxxx". In that way, when I 
want to use one of them (and the folder where they are stored is in the PATH), 
I just have to type "Script" and press *tab* two times to get a list of all 
custom scripts available
<br />
<br />
<br />

### Script-headex   (Python)

```
usage: Script-headex [-h] [-a action] --fasta fasta_file [-l]
                     [regexp [regexp ...]]

Pick/discard sequences from fasta file using regular expression matching the
headers

positional arguments:
  regexp                list of regular expressions that match headers
                        separated by spaces. If no regular expression is
                        provided all sequences will be selected.

optional arguments:
  -h, --help            show this help message and exit
  -a action             pick, discard DEFAULT: pick
  --fasta fasta_file, -f fasta_file
  -l

```
<br />
<br />
WARNING: Since is a Python script, regular expressions must be pythonic. They're
90% equal to bash regexp syntax but you must to be careful.
<br />
<br />
EXAMPLE:
<br />
<br />
If you want to pick all sequences having the word "tuberculosis" in the header
from "my_fasta_file.fasta"
<br />
<br />
`Script-headex -f "my_fasta_file.fasta" -a pick "tuberculosis"`
<br />
<br />
If you want to discard sequences with either "tuberculosis", "bovis" or "BCG" and
also want to consider that tuberculosis could be written with first cap:
<br />
<br />
`Script-headex -f "my_fasta_file.fasta" -a discard [Tt]uberculosis bovis BCG`
<br />
<br />
<br />
### Script-fasta-pick-coords   (Python)

```
usage: Script-fasta-pick-coords [-h] -f, --fasta fasta file -c, --coordinates
                                coordinates

Extract sequence from fasta given coordinates. A list of coordinates can be
provided and then a multifasta is generated as output

optional arguments:
  -h, --help            show this help message and exit
  -f, --fasta fasta file
  -c, --coordinates coordinates
```
NOTE: Coordinates are separated by semicolons ':'. When providing a list
of coordinates, each pair is separated by comma.
<br />
EXAMPLE: `Script-fasta-pick-coords -f "my_fasta.fasta" -c 123456:456789 > "my_output.fasta"`
<br />
<br />
<br />
### Script-fastq2fasta   (Bash)

```
usage: Script-fastq2fasta fastq_file

Convert fastq file in fasta file
```
<br />
EXAMPLE: `Script-fastq2fasta fastq_file > fasta_file`
<br />
<br />
<br />
### Script-sort-fasta   (Python - ridiculously fast)
```
usage: Script-sort-fasta [-h] -f FASTA [-a] [-o OUTPUT]

Given a multifasta file sort its sequences by size

optional arguments:
  -h, --help            show this help message and exit
  -f FASTA, --fasta FASTA
  -a, --ascending
  -o OUTPUT, --output OUTPUT
```
NOTE: If no output is specified, results are written to standard output (terminal)
<br />
EXAMPLE: `Script-sort-fasta -f "unsorted_fasta.fasta" -o "sorted_fasta.fasta"`
<br />
<br />
<br />
### Script-fasta-pick-readlist    (Python)
```
usage: Script-fasta-pick-readlist [-h] -in INPUTF -r READLIST
                                  [-a {discard,pick}] [-o OUTPUT]
                                  [-f {fastq,fasta}] [-c]

Given a file with reads identifiers (one each row), pick/discard those reads
from fasta/fastq file

optional arguments:
  -h, --help            show this help message and exit
  -in INPUTF, --inputf INPUTF
  -r READLIST, --readlist READLIST
  -a {discard,pick}, --action {discard,pick}
  -o OUTPUT, --output OUTPUT
  -f {fastq,fasta}, --format {fastq,fasta}
  -c, --compressed
```
EXAMPLE: We have a file called MTBCreads.list that contains reads ids that have
been classified as MTBC by any software and it looks like:

`M04376:71:000000000-ATW80:1:2106:16030:6208`<br />
`M04376:71:000000000-ATW80:1:2106:14450:6209`<br />
`M04376:71:000000000-ATW80:1:2106:16846:6209`<br />
`...`<br />
So we can pick these MTBC-classified reads with

`Script-fasta-pick-readlist -in runX.fastq.gz -f fastq -r MTBCreads.list --compressed -o MTBCreads.fastq`

NOTE: If no output is specified results are written to standard output (terminal)
<br />
<br />
<br />
### Script-distribute-files   (Python)

```
usage: Script-distribute-files [-h] -l [LIST OF FILES [LIST OF FILES ...]] -n
                               NUMBER OF FOLDERS [-p PREFIX] [-a {cp,mv}]

Distribute a list of files in n different folders

optional arguments:
  -h, --help            show this help message and exit
  -l [LIST OF FILES [LIST OF FILES ...]], --list [LIST OF FILES [LIST OF FILES ...]]
  -n NUMBER OF FOLDERS, --num-folders NUMBER OF FOLDERS
  -p PREFIX, --prefix PREFIX
  -a {cp,mv}, --action {cp,mv}
```
EXAMPLE: Take a large dataset of *.sort.bam files and evenly distribute them in 8 folders
by copying files in folders named as SortBam_1, SortBam_2...<br />
`Script-distribute-files -l $(ls -S *.sort.bam) -n 8 -p SortBam -a cp`
<br />
<br />
<br />
### Script-coverage-bed  (Python)

```
usage: Script-coverage-bed [-h] -c FILE.coverage [-b FILE.bed]

From a .coverage and a .bed file calculate mean and median coverage per bed
regions

optional arguments:
  -h, --help            show this help message and exit
  -c FILE.coverage, --coverage FILE.coverage
                        File with per-position coverage
  -b FILE.bed, --bedfile FILE.bed
```
The coverage file must have coverage values for each position.
The bed file is optional if running on the servers, as it takes a default bed in
"/data/Databases/MTB_ancestor/H37Rv/H37Rv.bed".

Ment to calc coverage values across annotated features

`Script-coverage-bed -c SRR5153094.coverage | head`

```
    Feature Mean    Q25     Q50     Q75     Kurtosis
    IG1521_Rv1985c_Rv1986   114.611111111   100.0   119.0   126.0   -0.99522286119
    IG1867_Rv2449c_Rv2450c  64.0112359551   61.0    62.0    64.0    0.87484953427
    sIG2727_Rv3594_Rv3595c  47.3260869565   45.0    46.0    49.0    0.0990856340825
    Rv0042c 102.129186603   93.0    100.0   113.0   -0.819193678711
    Rv0965c 91.8547619048   80.0    92.0    100.0   -0.374844879418
    Rv0422c 89.2731829574   78.0    89.0    100.0   0.200572622547
    Rv0455c 111.861297539   102.0   111.0   121.0   -0.428494519321
    sIG1424_Rv1857_Rv1858   99.5    99.25   99.5    99.75   nan
    IG2016_Rv2647_Rv2649    207.287878788   72.75   87.5    402.0   -1.77363410836 
```

### Script-mapping-stats

```
usage: Script-mapping-stats [-h] -s SAMFILE [-p PREFIX]

optional arguments:
  -h, --help            show this help message and exit
  -s SAMFILE, --samfile SAMFILE
  -p PREFIX, --prefix PREFIX
```
This script can ONLY be used with a very spefici sam file with only mapped reads
and matchs tagged as "=". So, if we want to calc mapping stats for my_file.bam:
```
samtools fillmd -e my_file.bam > my_file.mdsam
samtools view -S -F4 my_fyle.mdsam > my_file.mapmdsam
Script-mapping-stats -s my_file.mapmdsam -p my_sample
```
EXAMPLE OUTPUT: (_p means percentage)
```
Record  MAPQ    Matchs  Matchs_p        Mismatchs       Mismatchs_p     Deletions       Insertions      Clipping        Clipping_p      Prefix                                                                                               
SRR5153621.3    60      76      0.752475247525  1       0.00990099009901        0       0       24      0.237623762376  SRR5153621                                                                                                           
SRR5153621.4    60      58      0.574257425743  0       0.0     0       0       43      0.425742574257  SRR5153621                                                                                                                           
SRR5153621.6    60      25      0.301204819277  0       0.0     0       0       58      0.698795180723  SRR5153621                                                                                                                           
SRR5153621.7    60      39      0.39    0       0.0     0       0       61      0.61    SRR5153621                                                                                                                                           
SRR5153621.8    60      81      0.80198019802   0       0.0     0       0       20      0.19801980198   SRR5153621                                                                                                                           
SRR5153621.10   60      72      0.712871287129  0       0.0     0       0       29      0.287128712871  SRR5153621                                                                                                                           
SRR5153621.11   60      32      0.421052631579  1       0.0131578947368 0       0       43      0.565789473684  SRR5153621                                                                                                                   
SRR5153621.12   60      54      0.534653465347  0       0.0     0       0       47      0.465346534653  SRR5153621                                                                                                                           
SRR5153621.13   60      72      0.712871287129  0       0.0     0       0       29      0.287128712871  SRR5153621  
```

### Script-SNP-summmary
```
usage: Script-SNP-summary [-h] -s SNP_FILE [-c .meancov file]
                          [-n .nfilter file] [--sample Sample name]

Produce summary stats from TBgroup SNP files

optional arguments:
  -h, --help            show this help message and exit
  -s SNP_FILE, --snp SNP_FILE
  -c .meancov file, --coverage .meancov file
  -n .nfilter file, --nreads .nfilter file
  --sample Sample name
```

`Script-SNP-summary -s SRR5153621.DR.snp.final`
```
Sample  reads   perc_MTBC       coverage        n_snps  homo    hetero  perc_homo       perc_hetero     freqs-heteroknown_res_snp   unknown_res_snp
SRR5153621      NA      NA      NA      1829    1135    694     0.620557681793  0.379442318207  8.97-22.5875-34.02-54.85-87.72      12      11
```
meancov and nfilter files can be provided so reads, perc_MTBC and coverage are 
reported as well

### Script-SNP-mergesnp ("The completely useless table" - *Someone*, 1st December 2017)
```
usage: Script-SNP-mergesnp [-h] -s
                           [SNP_FILES) to merge [SNP_FILE(S to merge ...]]

Merge SNP files produced in TGU in a single table summarizing positions for
all samples within a directory

optional arguments:
  -h, --help            show this help message and exit
  -s [SNP_FILE(S) to merge [SNP_FILE(S) to merge ...]], --snp [SNP_FILE(S) to merge [SNP_FILE(S) to merge ...]]
```
Use wildcards to select which files to merge as you'd do it in bash

EXAMPLE:
`Script-SNP-mergesnp -s *DR.snp.final | head `
```
Position        Ref     VarAllele       SRR5153614      SRR5153619      SRR5153621
1849    C       A       99,33%  99,24%  99,6%
2532    C       T       100%    98,95%  100%
4086    G       T       NA      45,58%  29,47%
7582    A       G       100%    99,39%  99,41%
9143    C       T       100%    98,54%  99,12%
11820   C       G       99,48%  99,49%  99,16%
13460   G       A       99,53%  99,43%  98,68%
14401   G       A       99,43%  99,33%  99,28%
14861   G       T       99,22%  99,49%  99,51%
```

### Script-SNP-filter-mergesnp
```
usage: Script-SNP-filter-mergesnp [-h] -s MERGED_SNP_TABLE
                                  [--min-strains MINS] [--max-strains MAXS]
                                  [--max-perc-strains MAXPERC]
                                  [--min-perc-strains MINPERC]
                                  [--n-over NUM:FREQ] [--n-below NUM:FREQ]
                                  [--perc-over PERC:FREQ]
                                  [--perc-below PERC:FREQ]
Script-SNP-filter-mergesnp: error: argument -s/--snp_file is required
```
Filter a mergesnp table generated with `Script-SNP-mergesnp` according to 
different parameters:
```
--min/max-strains : min and max number of STRAINS that position has been called for
--min/max--perc--strains: min and max percentage of STRAINS that position has been called for
--n-over/below: number of CALLS with a frequency over/below FREQ for that variant
--perc-over/below: percentage of CALLS with a frequency over/below FREQ for that variant
```
EXAMPLE: let's get those positions that have been called in at least the fifty
percent of the dataset with a frequency below 90% in at least the 60% of the calls

`Script-SNP-filter-mergesnp -s my_table.mergesnp --min-perc-strains 50 --perc-below 60:90`

### Script-SNP-tag-resistance-positions
```
usage: Script-SNP-tag-resistance-positions [-h] -s File to parse positions
                                           from -f Field/Column of positions
                                           [-H]
                                           [--separator separator character. DEFAULT=TAB]
                                           [-b bed file with resistance associated genes]

From a file with any column describing MTB positions, read from
/data/Databases/ResistanceData files and add columns pointing positions
associated to resistance

optional arguments:
  -h, --help            show this help message and exit
  -s File to parse positions from, --snp-file File to parse positions from
  -f Field/Column of positions, --field Field/Column of positions
  -H, --header
  --separator separator character. DEFAULT=TAB
  -b bed file with resistance associated genes, --bed bed file with resistance associated genes
```
It just needs a file where any column has positions. It is necessary to specify
which column has that information. If the file has a header, then option -H has to be
specified. By default it reads from a tab-separated file, but different separator
can be specified. By default reads resistance-associated genes from 
/data/Databases/ResistanceData/ResistanceGenes.bed and known resistance mutations
from /data/Databases/ResistanceData/phyresse_full_list_modified.csv. A different
bed file can be provided to tag other genes, but some columns of the output will 
always be related to resistance information:

Example: From a DR.snp.final, which has a header (-H) and positions in second column (-f 2)<br />
`Script-SNP-tag-resistance-positions -s SRR5153621.DR.snp.final -f 2 -H | head`

```
Chrom   Position        Ref     Cons    Reads1  Reads2  VarFreq Strands1        Strands2        Qual1   Qual2   Pvalue      MapQual1        MapQual2        Reads1Plus      Reads1Minus     Reads2Plus      Reads2Minus     VarAllele  Resistance       Known   Gene    Drug
MTB_anc 1849    C       A       0       249     99,6%   0       2       0       45      0.98    0       1       0  0162     87      A       FALSE   NA      NA      NA
MTB_anc 2532    C       T       0       89      100%    0       2       0       45      0.98    0       1       0  047      42      T       FALSE   NA      NA      NA
MTB_anc 4086    G       K       134     56      29,47%  2       2       47      39      0.98    1       1       57 77       20      36      T       FALSE   NA      NA      NA
MTB_anc 7582    A       G       0       168     99,41%  0       2       0       40      0.98    0       1       0  067      101     G       TRUE    TRUE    gyrA    FQ
MTB_anc 9143    C       T       0       112     99,12%  0       2       0       44      0.98    0       1       0  058      54      T       TRUE    FALSE   gyrA    FQ
MTB_anc 11820   C       G       1       237     99,16%  1       2       27      48      0.98    1       1       1  0163     74      G       FALSE   NA      NA      NA
MTB_anc 13460   G       A       2       149     98,68%  1       2       37      41      0.98    1       1       0  286      63      A       FALSE   NA      NA      NA
MTB_anc 14401   G       A       0       137     99,28%  0       2       0       45      0.98    0       1       0  075      62      A       FALSE   NA      NA      NA
MTB_anc 14861   G       T       1       203     99,51%  1       2       27      41      0.98    1       1       0  1107     96      T       FALSE   NA      NA      NA
```

Columns added are:
```
Resistance -> Is it a gene associated to resistance?
Known -> Is it a known POSITION (It should be mutation instead) from phyresse?
Gene -> Which gene
Drug -> What drug confers resistance to
```

### Script-SNP-filter-by-annotation
**NOTE: this script is also implemented under ThePipeline annotation_filter**
```
usage: Script-SNP-filter-by-annotation [-h] -s File to parse positions from -f
                                       Field/Column of positions [-H]
                                       [--separator separator character. DEFAULT=TAB]

From a file with any column describing MTB positions, read from
/data/Databases/MTB_annotation/H37Rv.annotation.tab and filter those positions
tagged as DISCARD in annotation file

optional arguments:
  -h, --help            show this help message and exit
  -s File to parse positions from, --snp-file File to parse positions from
  -f Field/Column of positions, --field Field/Column of positions
  -H, --header
  --separator separator character. DEFAULT=TAB
```
The default annotation file has tagged as DISCARDED all those positions that are
annotated as repeat, PPE, phage or its associated intergenic regions.

For example I want to filter MY_SAMPLE.snp file, which has positions on second
field. As it has a header, I have to specify it with -H:
`Script-SNP-filter-by-annotation -s MY_SAMPLE.snp -f 2 -H`

### Script-coverage-calculate-windows
```
usage: Script-coverage-calculate-windows [-h] -c .coverage file
                                         [--calc median/mean] -w BP
                                         [-d DELIMITER]
```
Calculate mean or median coverage in windows defined by user given a .coverage file

```
Script-coverage-calculate-windows -c MZ19.coverage -w 1000 | head
MTB_anc 1:1000  68.501
MTB_anc 1001:2000       63.56
MTB_anc 2001:3000       33.698
MTB_anc 3001:4000       87.562
MTB_anc 4001:5000       58.807
MTB_anc 5001:6000       77.45
MTB_anc 6001:7000       63.852
MTB_anc 7001:8000       56.902
MTB_anc 8001:9000       68.593
MTB_anc 9001:10000      61.676
```

### Script-get-clusters
```
usage: Script-get-clusters [-h] -d genetic_distances file
                           [-t SNP threshold. DEF: 15] [--sep [Space|Tab]]
                           [--output-senyorito-irving]

Identify clusters given a snp threshold

optional arguments:
  -h, --help            show this help message and exit
  -d genetic_distances file, --distances genetic_distances file
  -t SNP threshold. DEF: 15, --threshold SNP threshold. DEF: 15
  --sep [Space|Tab]
  --output-senyorito-irving

```

EXAMPLE
```
Script-get-clusters -d genetic_distances | head

Cluster Samples
Cluster_1       G945,G1023
Cluster_2       G108,G1320,G260,G99,G1350
Cluster_3       G1090,G271
Cluster_4       G1562,G1128
Cluster_5       G171,G1165,G768,G1161
Cluster_6       G782,G1192
Cluster_7       G1269,G1355
Cluster_8       G1303,G623,G778
Cluster_9       G1310,G1309

Script-get-clusters -d distances/genetic_distances --output-senyorito-irving
Cluster Samples
Cluster_1       ERR1352301
Cluster_1       ERR1352300
Cluster_1       ERR1352302
Cluster_1       ERR1352299
Cluster_1       ERR1352298
Cluster_2       ERR1352303
Cluster_2       ERR1352305
Cluster_2       ERR1352304
Cluster_2       ERR1352306
Cluster_3       ERR1352309
Cluster_3       ERR1352308
Cluster_3       ERR1352310
Cluster_3       ERR1352307
```

### Script-sort-fasta (Python)
Sort by sequence length (ascending or descending) a multifasta file.

Quite fast, btw =)

```
usage: Script-sort-fasta [-h] -f FASTA [-a] [-o OUTPUT]

Given a multifasta file sort its sequences by size

optional arguments:
  -h, --help            show this help message and exit
  -f FASTA, --fasta FASTA
  -a, --ascending
  -o OUTPUT, --output OUTPUT
```
