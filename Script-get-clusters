#! /usr/bin/python

# Galo Adrian Goig 9 de Febrero de 2018
# galo.goig@uv.es
# @Adrian_Goig

# This is script is thought to get as input a file of type
# "genetic_distances" with following structure
#
#
# "V1" "V2" "V3"
# "ERR1679588" "803" "G1986"
# "ERR1679585" "2121" "ERR1679588"
# "ERR1679587" "2146" "ERR1679588"
# "ERR1679586" "2209" "ERR1679588"
#
# IMPORTANTLY, quotes MUST BE REMOVED in the input file

def parse_args():
    import argparse

    parser = argparse.ArgumentParser(description="Identify clusters given a snp threshold")
    parser.add_argument("-d", "--distances", metavar="genetic_distances file", dest="distfile",
        required = True)
    parser.add_argument("-t", "--threshold", metavar="SNP threshold. DEF: 15", default=15,
        type=int, dest="threshold")
    parser.add_argument("--sep", metavar="[Space|Tab]", default="Space", dest="sep",
        choices={"Space", "Tab"})
    parser.add_argument("--output-senyorito-irving", "-osi", dest="output", action="store_true")

    args = parser.parse_args()

    return args

def ParseDistances(distfile, sep):

    with open(distfile) as infile:
        header = infile.readline()
        if sep == "Space":
            for line in infile:
                line = line.rstrip().split()
                yield line
        elif sep == "Tab":
            for line in infile:
                line = line.rstrip().split("\t")
                yield line

def getClusters(args):

    clusters = []
    for A, dist, B in ParseDistances(args.distfile, args.sep):
        clustered = False
        dist = int(dist)
        if dist <= args.threshold:
            if clusters:
                samples_present_in = []
                for i in range(len(clusters)):
                    cluster = clusters[i]
                    if A in cluster:
                        cluster.append(B)
                        samples_present_in.append(i)
                        clustered = True

                    elif B in cluster:
                        clustered = True
                        cluster.append(A)
                        samples_present_in.append(i)

                if not clustered:
                    newcluster = [A, B]
                    clusters.append(newcluster)

                elif len(samples_present_in) > 1:
                    newcluster = []
                    offset = 0
                    for i in samples_present_in:
                        newcluster.extend(clusters[i-offset])
                        del clusters[i-offset]
                        offset += 1
                    clusters.append(newcluster)

            else:
                clusters.append( [A,B])
        else:
            if clusters:
                A_present_in = []
                for i in range(len(clusters)):
                    cluster = clusters[i]
                    if A in cluster:
                        A_present_in.append(i)
                        clustered = True

                if not clustered:
                    clusters.append([A])

                elif len(samples_present_in) > 1:
                    newcluster = []
                    offset = 0
                    for i in A_present_in:
                        newcluster.extend(clusters[i-offset])
                        del clusters[i-offset]
                        offset += 1
                    clusters.append(newcluster)

                clustered = False
                B_present_in = []
                for i in range(len(clusters)):
                    cluster = clusters[i]
                    if B in cluster:
                        B_present_in.append(i)
                        clustered = True

                if not clustered:
                    clusters.append([B])

                elif len(samples_present_in) > 1:
                    newcluster = []
                    offset = 0
                    for i in B_present_in:
                        newcluster.extend(clusters[i-offset])
                        del clusters[i-offset]
                        offset += 1
                    clusters.append(newcluster)

            else:
                clusters.append([A])
                clusters.append([B])

    return clusters

def deDupClusters(clusters):
    dedup = []
    for cluster in clusters:
        cluster = set(cluster)
        cluster = list(cluster)
        dedup.append(cluster)
        
    return dedup 

def writeClusters(clusters):
    import sys

    cont = 1
    sys.stdout.write("Cluster\tSamples\n")
    for cluster in clusters:
        line = "Cluster_{}\t".format(cont)
        line += "{}".format(cluster[0])
        for sample in cluster[1:]:
            line += ",{}".format(sample)
        line += "\n"
        sys.stdout.write(line)
        cont += 1

def writeClustersIrving(clusters):
    import sys

    cont = 1
    sys.stdout.write("Cluster\tSamples\n")
    for cluster in clusters:
        line = "Cluster_{}\t".format(cont)
        for sample in cluster:
            sys.stdout.write("{}{}\n".format(line, sample))
        cont += 1

def main():
    args = parse_args()
    clusters = getClusters(args)
    clusters = deDupClusters(clusters)
    if not args.output:
        writeClusters(clusters)
    else:
        writeClustersIrving(clusters)


if __name__ == "__main__":
    main()