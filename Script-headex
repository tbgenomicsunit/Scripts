#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  
#   This program picks fasta sequences according to a list of regular
#   expressions matching any part of the header.
#   INPUT: Fasta + regex list + action; where action could be either,
#   write those sequences that match or those that don't.
#   OUTPUT: Fasta
#   SYSTEM REQUIREMENTS: UNIX / BIOPYTHON

def parse_args():
    import argparse
    parser = argparse.ArgumentParser(description="Pick/discard sequences from \
    fasta file using regular expression matching the headers")
    parser.add_argument("-a", dest="action", 
                         metavar="action", default="pick", 
                        choices={"pick", "discard"}, help="pick, discard \
                        DEFAULT: pick")
    parser.add_argument(dest="regexp_list", metavar="regexp", nargs="*", 
                         help="list of regular expressions that match \
                              headers separated by spaces. If no regular\
                              expression is provided all sequences will \
                              be selected.") 
    parser.add_argument("--fasta", "-f", metavar="fasta_file", dest="fasta", required=True)
    parser.add_argument("-l", dest="literal", action="store_true")
    args = parser.parse_args()

    return args

def regexp_check(regexp_list):
    '''This function compiles all regular expressions provided by user 
    in order to check if they're valid mainly'''
    import re
    import sys
    
    regular_expressions = []
    for regexp in regexp_list:
        try: 
            regular_expressions.append(re.compile(regexp))
        except:
            sys.stderr.write("\nOne or more regular expressions are invalid, at least in Python. Check them out!\n")
            sys.exit(1)
    return regular_expressions

def pick_seqs(fasta, action, regexp_list):
    '''This function search regular expressions through the headers of
    the fasta file and writes those that match or those that don't
    according to action set by user'''
    from Bio import SeqIO
    import re
    import sys
    
    if action == "pick":
        with open(fasta) as infile:
            for record in SeqIO.parse(infile, "fasta"):
                for regexp in regexp_list:
                    if re.search(regexp, record.description):
                        SeqIO.write(record, sys.stdout, "fasta")
                        break
    if action == "discard":
        with open(fasta) as infile:
            for record in SeqIO.parse(infile, "fasta"):
                coincidences = False
                for regexp in regexp_list:
                    if re.search(regexp, record.description):
                        coincidences = True
                        break
                if not coincidences:
                    SeqIO.write(record, sys.stdout, "fasta")

def main():
    
    args = parse_args()
    regexp_list = args.regexp_list

    if not args.literal:
    	regexp_list = regexp_check(regexp_list)
 
    pick_seqs(args.fasta, args.action, regexp_list)
    return 0

if __name__ == '__main__':
    main()

