#!/usr/bin/env python 

def parse_args():
    '''Parse arguments given to script'''

    import argparse

    parser = argparse.ArgumentParser(description="Distribute a list of files in n different "\
        "folders")
    parser.add_argument("-l", "--list", dest="file_list", metavar="LIST OF FILES",
        required = True, nargs="*")
    parser.add_argument("-n", "--num-folders", dest="n", metavar="NUMBER OF FOLDERS",
        required = True)
    parser.add_argument("-p", "--prefix", dest="prefix", default="Folder")
    parser.add_argument("-a", "--action", dest="action", default="cp", choices={"cp", "mv"})
    
    args = parser.parse_args()

    return args

def FolderList(prefix, n):
    '''Return a list of n folder names with prefix_num'''
    
    folder_list = ["{}_{}/".format(prefix, i+1) for i in range(n)]
    
    return folder_list

def NextFolder(folder_list):
    '''Generator that yields folders from folder_list infinitely'''
    while True:
        folder_list.append(folder_list.pop(0))
        yield folder_list[-1]

def DistributeFiles(file_list, folder_list, action):
    '''Create folders from folder list and distribute files among these 
    one by one'''
    import os
    from os.path import realpath
    import shutil

    # Create all folders
    for folder in folder_list:
        os.mkdir(folder)

    # Get the generator
    next_folder = NextFolder(folder_list)
    for file in file_list:
        target_folder = next_folder.next()
        if action == "cp":
            shutil.copyfile(realpath(file), "{}{}".format(target_folder, file))
        elif action == "mv":
            shutil.move(realpath(file), target_folder)

def main():

    args = parse_args()
    folder_list = FolderList(args.prefix, int(args.n))
    DistributeFiles(args.file_list, folder_list, args.action)

if __name__ == "__main__":
    main()